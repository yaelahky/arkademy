import React from 'react';
import logo from './logo.jpg';
import './App.css';
import { Header, Image, Table, Modal, Button, Icon} from 'semantic-ui-react'

function App() {
  return (
    <div className="App">
      <Header as='h2' className="header">
        <Image className="header-image" circular src={logo} />
        <div className="header-name">Arkademy Online Store</div>
      </Header>
      <div className="wrapper-data">
      <Table singleLine>
        
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Nama</Table.HeaderCell>
        <Table.HeaderCell>Pesanan</Table.HeaderCell>
        <Table.HeaderCell>Ubah Pesanan</Table.HeaderCell>
      </Table.Row>
    </Table.Header>

    <Table.Body>
      <Table.Row>
        <Table.Cell>KIKY</Table.Cell>
        <Table.Cell>Roti</Table.Cell>
        <Table.Cell>
        <Modal trigger={<Button color='red'>Hapus</Button>} closeIcon>
          <Header icon='archive' content='Archive Old Messages' />
          <Modal.Content>
            <p>
              Your inbox is getting full, would you like us to enable automatic
              archiving of old messages?
            </p>
          </Modal.Content>
          <Modal.Actions>
            <Button color='red'>
              <Icon name='remove' /> No
            </Button>
            <Button color='green'>
              <Icon name='checkmark' /> Yes
            </Button>
          </Modal.Actions>
        </Modal>
        </Table.Cell>
      </Table.Row>
    </Table.Body>
  </Table>
      </div>
    </div>
  );
}

export default App;
